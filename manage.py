#!/usr/bin/env python
import os
import sys
import platform

if __name__ == "__main__":
    if platform.system() == 'Linux':
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.development")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

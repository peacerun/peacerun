from django.http import Http404
from django.shortcuts import redirect
from django.template import TemplateDoesNotExist
from django.template.loader import select_template
from django.views.generic import TemplateView
from .utils import get_names


class PageTemplateView(TemplateView):

    def get_url(self):
        """Get the path from the url route.
        The path can also be passed in through a keyword argument, `url`.
        :returns: The URL path for the requested page.

        """
        if getattr(self, 'url', None):
            return self.url

        url = self.kwargs.get('page_url', None)
        if url is None:
            raise Http404("URL not provided as class argument or kwarg")

        if url in ("/", ""):
            return "index"

        return url

    def get_template_names(self):
        """Get the template name that should be used for this page.

        This will use `get_page_names()` to find a list of valid template names
        that could be used for the page. It will then return the first one
        that is found.

        Note: This method will append ".html" to the page names returned by
        `get_page_names()`.

        Example:

        If you have a url as ``/example/leaf/url/``, the following templates
        will be tried:

        1. example/leaf/url.html
        2. example/leaf/url/index.html
        3. pages/example/leaf/url.html
        4. pages/example/leaf/url/index.html

        """

        template_names = [u"{0}.html".format(p) for p in get_names(self.get_url())]

        try:
            t = select_template(template_names)

            if hasattr(t, 'template'):
                # For django >= 1.8
                return [t.template.name]
            else:
                # For django < 1.8
                return [t.name]
        except (TemplateDoesNotExist, UnicodeEncodeError):
            raise Http404(u"Template could not found. Tried: {0}".format(", ".join(template_names)))

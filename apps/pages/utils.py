import os
from functools import partial

from django.shortcuts import render
from django.template.loader import render_to_string


def get_names(url):
    """Get a list of valid page names.

    Valid page names will be a list of two types:

    1. The full page name provided
    2. The full page name provided starting with `pages/`

    Page names can also be used as the folder name, so index.html files
    will also work.

    Example:

    If the url is ``/example/leaf/url/``, the following page names will
    be returned:

    1. example/leaf/url
    2. example/leaf/url/index
    3. pages/example/leaf/url
    4. pages/example/leaf/url/index

    :param str url: The page URL (without extension)
    :returns: A list of valid template names (without extension) to look for

    """
    pages = []

    if url.startswith('admin'):
        return []

    pages.append(url)
    pages.append(os.path.join(url, 'index'))
    pages.append(os.path.join('pages', url.strip('/')))
    pages.append(os.path.join('pages', url.strip('/'), 'index'))

    return [p.rstrip('/') for p in pages]


# Always use the Django template engine on Django 1.8.
render_to_string = partial(render_to_string, using='django')
render = partial(render, using='django')
from .admin import PagesAdminMiddleware
from .mixins import TranslationMixin


class PagesTranslationMiddleware(PagesAdminMiddleware, TranslationMixin):

    def process_request(self, request):
        super().process_request(request)
        self.activate_language()

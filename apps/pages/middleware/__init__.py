import cio
from cio.pipeline import pipeline
from django.utils.deprecation import MiddlewareMixin


class PagesMiddleware(MiddlewareMixin):

    def process_request(self, request):
        # Bootstrap content-io
        pipeline.clear()
        cio.env.reset()

    def process_response(self, request, response):
        pipeline.clear()
        return response

    def process_exception(self, request, exception):
        pipeline.clear()

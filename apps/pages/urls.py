"""urlconf for the pages application"""

from django.conf.urls import url, include
from .views import PageTemplateView

app_name='pages'

urlpatterns = [
    url(r'^pages/', include('pages.admin.urls')),
    url(r'(?P<page_url>.*)(/)?$', PageTemplateView.as_view(), name='page')
]

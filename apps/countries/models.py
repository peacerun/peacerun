from django.db import models
from django.utils.translation import ugettext_lazy as _


class CountryManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(active=True, **kwargs)

    def default(self, **kwargs):
        return self.get(default=True, **kwargs)


class Country(models.Model):
    id = models.CharField('ISO 3166-1 Alpha 2 Name', max_length=2, primary_key=True)
    name = models.CharField('Country Name', max_length=128, unique=True)
    active = models.BooleanField(default=False)
    default = models.NullBooleanField(unique=True)  # in this way only one record with True value is allowed

    objects = CountryManager()

    def __str__(self):
        return self.name

    class Meta(object):
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

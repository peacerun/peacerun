# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-08-07 21:52
from __future__ import unicode_literals

from django.db import migrations

def create_data(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Country = apps.get_model("countries", "Country")
    db_alias = schema_editor.connection.alias
    Country.objects.using(db_alias).bulk_create([
        Country(name="USA", id="US", active=True, default=True),
        Country(name="Australia", id="AU", active=True),
        Country(name="Canada", id="CA", active=True),
        Country(name="France", id="FR", active=True),
        Country(name="Slovakia", id="SK", active=True),
        Country(name="Czech republic", id="CZ", active=True),
    ])

def remove_data(apps, schema_editor):
    # forwards_func() creates two Country instances,
    # so reverse_func() should delete them.
    Country = apps.get_model("countries", "Country")
    db_alias = schema_editor.connection.alias
    Country.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('countries', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_data, remove_data),
    ]

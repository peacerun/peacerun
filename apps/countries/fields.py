from django.db.models import ForeignKey


class CountryField(ForeignKey):
    
    def __init__(self, *args, to=None, **kwargs):
        super().__init__(to='countries.Country', *args, **kwargs)
        
from .models import Country
from .utils import geolocate_ip


def country_middleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):

        country_code = geolocate_ip(request)
        if not country_code:
            country_code = Country.objects.default()
        country_code = geolocate_ip(request)
        request.COUNTRY_CODE = country_code

        response = get_response(request)

        return response

    return middleware
from contextlib import closing
import json
from urllib.request import urlopen
from django.conf import settings


def get_user_ip(request):
    meta = request.META
    return meta.get('HTTP_X_REAL_IP',
                    meta.get('HTTP_CLIENT_IP',
                             meta.get('HTTP_X_FORWARDED_FOR',
                                      meta.get('REMOTE_ADDR'))))


def geolocate_ip(request, url='http://freegeoip.net/json/'):
    """ Automatically geolocate the given IP address """
    ip = get_user_ip(request)
    if ip and ip not in settings.INTERNAL_IPS:
        url = url + ip
        try:
            with closing(urlopen(url)) as response:
                location = json.loads(response.read().decode())
            return location['country_code']
        except Exception as e:
            pass
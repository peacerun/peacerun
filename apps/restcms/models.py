from django.db import models
from django.utils.translation import ugettext_lazy as _


class TextBlock(models.Model):

    name = models.CharField(_('Name'), max_length=100)
    text = models.TextField(_('Text'))



class Translation(models.Model):
    
    block = models.ForeignKey(TextBlock)
    language = models.ForeignKey('languages.language', verbose_name=_('Language'))
    text = models.TextField(_('Text'))
    
    #: text (e.g. "en-US", see https://en.wikipedia.org/wiki/IETF_language_tag)
from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from .views import TextBlockViewSet, TranslationViewSet


router = DefaultRouter()

router.register(r'text', TextBlockViewSet)
router.register(r'translation', TranslationViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
]
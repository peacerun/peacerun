from rest_framework import viewsets
from .models import TextBlock, Translation
from .serializers import TextBlockSerializer, TranslationSerializer


class TextBlockViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """
    queryset = TextBlock.objects.all()
    serializer_class = TextBlockSerializer
    
    
class TranslationViewSet(viewsets.ModelViewSet):
    
    queryset = Translation.objects.all()
    serializer_class = TranslationSerializer
from rest_framework import serializers
from .models import TextBlock, Translation


class TextBlockSerializer(serializers.HyperlinkedModelSerializer):    
    
    class Meta:
        model = TextBlock
        fields = ('id', 'url', 'name', 'text')
        extra_kwargs = {
            'url': {'lookup_field': 'id'},
        }


class TranslationSerializer(serializers.HyperlinkedModelSerializer):

    language = serializers.CharField(source='language_id')

    class Meta:
        model = Translation
        fields = ('id', 'block', 'language', 'text')
        extra_kwargs = {
            'url': {'lookup_field': 'id'},
        }    
    
from django.contrib import admin
from .models import TextBlock, Translation



class TranslationInline(admin.StackedInline):
    
    model = Translation
    fields = ('language', 'text')
    extra = 1


class TextBlockAdmin(admin.ModelAdmin):
    
    list_display = ('id', 'name')
    inlines = (TranslationInline,)


admin.site.register(TextBlock, TextBlockAdmin)
"""
    from http://djangosnippets.org/snippets/1049/
"""
import urllib, codecs
import csv
import io
import urllib.request
from django.core.management.base import BaseCommand
from django.db.utils import DataError
from ...models import Language


DATA_URL = "https://raw.githubusercontent.com/datasets/language-codes/master/data/language-codes.csv"

def decode_line(line):
    line = line.replace('"', '')
    print(line)
    value, name = line.split(',')
    return value, name
    

def get_language_data():
    "Returns a list of dictionaries, each representing a country"
    
    
    webpage = urllib.request.urlopen(DATA_URL)
    datareader = csv.reader(io.TextIOWrapper(webpage))

    headers = []
    for i, row in enumerate(datareader):
        if i == 0:
            headers = row
            # insert blank row
            yield {'alpha2': "--", 'English': "N/A"}
        else:
            yield dict(zip(headers, row))            
    
#     return data
#         
#     
#     with urllib.request.urlopen(DATA_URL) as response:
#         udata = response.read().decode('utf-8')
# 
#     # Strip the BOM
#     if udata[0] == codecs.BOM_UTF8.decode('utf8'):
#         udata = udata[1:]
#     # Ignore blank lines
#     lines = [l for l in udata.split('\n') if l]
#     
#     # First line has the headers 
#     header_line = lines[0]
#     headers = decode_line(header_line)
# 
# 
#     for line in lines[1:]:
#         yield dict(zip(headers, decode_line(line)))


class Command(BaseCommand):

    def handle(self, *args, **options):

        qs = Language.objects.all()
        for line in get_language_data():
            
            id = line['alpha2']
            data = dict(name = line['English'])

            try:
                obj, created = qs.get_or_create(id=id, defaults=data)
                print(obj)
                if not created:
                    qs.filter(pk=obj.pk).update(**data)
            except DataError:
                self.stdout.write('id: %s, data: %r' % (id,data))                
                raise
                
import locale, os, sys
from django.conf import settings
from django.utils import lru_cache
from django.utils.encoding import smart_str
from django.utils.translation.trans_real import (get_language_from_path,
    parse_accept_lang_header, language_code_re, language_code_prefix_re)
from .models import Language

_default = None
_supported = []


def get_default_language():
    """
    Detects the default language from the database.
    If no default language is present, the default
    settings.LANGUAGE_CODE is used.

    This will reload its values in the context of a new thread.
    """
    global _default

    if _default is None:
        try:
            _default = smart_str(Language.objects.default().id)
        except Language.DoesNotExist:
            _default = settings.LANGUAGE_CODE

    return _default


def get_supported_languages():
    """
    Retrieve the supported languages.
    """
    global _supported

    if not _supported:

        _supported = [smart_str(l) for l in Language.objects.active().values_list('id', flat=True)]

        #if no languages are set use the default language
        if not _supported:
            _supported = [settings.LANGUAGE_CODE]

    return _supported


@lru_cache.lru_cache(maxsize=1000)
def get_supported_language_variant(lang_code, strict=True):
    """
    Returns the language-code that's listed in supported languages, possibly
    selecting a more generic variant. Raises LookupError if nothing found.

    If `strict` is False (the default), the function will look for an alternative
    country-specific variant when the currently checked is not found.

    lru_cache should have a maxsize to prevent from memory exhaustion attacks,
    as the provided language codes are taken from the HTTP request. See also
    <https://www.djangoproject.com/weblog/2007/oct/26/security-fix/>.
    """
    if lang_code:
        #retrieve list of supported languages
        supported = get_supported_languages()

        # If 'fr-ca' is not supported, try special fallback or language-only 'fr'.
        possible_lang_codes = [lang_code]

        generic_lang_code = lang_code.split('-')[0]
        if generic_lang_code != lang_code:
            possible_lang_codes.append(generic_lang_code)

        for code in possible_lang_codes:
            if code in supported:
                return code

        if not strict:
            # if fr-fr is not supported, try fr-ca.
            for supported_code in supported:
                if supported_code.startswith(generic_lang_code + '-'):
                    return supported_code
    raise LookupError(lang_code)

def get_language_from_path(path, strict=True):
    """
    Returns the language-code if there is a valid language-code
    found in the `path`.

    If `strict` is False (the default), the function will look for an alternative
    country-specific variant when the currently checked is not found.
    """
    regex_match = language_code_prefix_re.match(path)
    if not regex_match:
        return None
    lang_code = regex_match.group(1)
    try:
        return get_supported_language_variant(lang_code, strict=strict)
    except LookupError:
        return None


def get_language_from_request(request, check_path=False):
    """
    This method is used as a replacement to the original django language
    detection algorithm.

    User's language preference is determined by following order:
    1. language prefix in the url
    2. django_language key in user session
    3. django_language cookie
    4. Accept-Language HTTP header
    5. default language from db table

    Analyzes the request to find what language the user wants the system to
    show. Only languages listed as active in language table are taken into account.
    If the user requests a sublanguage where we have a main language, we send
    out the main language.

    If check_path is True, the URL path prefix will be checked for a language
    code, otherwise this is skipped for backwards compatibility.
    """
    #retrieve list of supported languages
    supported = get_supported_languages()

    if check_path:
        lang_code = get_language_from_path(request.path_info)
        if lang_code is not None:
            return lang_code

    if hasattr(request, 'session'):
        lang_code = request.session.get('django_language', None)
        if lang_code in supported and lang_code is not None:
            return lang_code

    lang_code = request.COOKIES.get(settings.LANGUAGE_COOKIE_NAME)

    try:
        return get_supported_language_variant(lang_code)
    except LookupError:
        pass

    accept = request.META.get('HTTP_ACCEPT_LANGUAGE', '')
    for accept_lang, unused in parse_accept_lang_header(accept):
        if accept_lang == '*':
            break

        if not language_code_re.search(accept_lang):
            continue

        if accept_lang and accept_lang in supported:
            return accept_lang

        try:
            return get_supported_language_variant(accept_lang)
        except LookupError:
            continue


    return get_default_language()


def get_country_language(country_code, lang_code):
    supported = get_supported_languages()
    # if there is no doubt about lang code return that
    if lang_code != get_default_language() and\
        len([lng for lng in supported if lng.startswith(lang_code)]) == 1:
        return lang_code

    if country_code:
        lang_codes = Language.objects.active(countries=country_code).values_list('id', flat=True)

        for lng in lang_codes:
            if lng.startswith(lang_code):
                # we are in the country where they speak our language
                return lng
        # this may be controversial
        if lang_code == get_default_language() and lang_codes:
            # first language in the country
            return lang_codes[0]

    # stay with the language set beforehand
    return lang_code


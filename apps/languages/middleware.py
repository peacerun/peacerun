import re
from django.conf import settings
from django.conf.urls.i18n import is_language_prefix_patterns_used
from django.core.urlresolvers import is_valid_path
from django.http import HttpResponsePermanentRedirect
from django.utils import translation
from django.utils.cache import patch_vary_headers
from .utils import get_language_from_request, get_default_language, get_country_language


def language_middleware(get_response):

    def middleware(request):

        """
        If the language code from request results in default language or generic
        language that has multiple occurences in different regions, use country
        language.
        """

        urlconf = getattr(request, 'urlconf', settings.ROOT_URLCONF)
        i18n_patterns_used, prefixed_default_language = is_language_prefix_patterns_used(urlconf)
        language = get_language_from_request(request, check_path=i18n_patterns_used)
        if getattr(request, 'COUNTRY_CODE', None):
            language = get_country_language(request.COUNTRY_CODE, language)

        translation.activate(language)
        request.LANGUAGE_CODE = translation.get_language()

        response = get_response(request)

        process_response(request, response, language, i18n_patterns_used, urlconf)

        return response

    return middleware


def process_response(request, response, language, i18n_patterns_used, urlconf):
    """
    Override the original method to redirect permanently and facilitate
    the :func.`translations.urls.translation_patterns` URL redirection
    logic.
    """

    default = get_default_language()

    #redirect to the original default language URL
    #if language prefix is used
    if (response.status_code == 404 and
        i18n_patterns_used and default == language and
        request.path_info.startswith('/%s/' % default)):

        language_path = re.sub(r'^/%s/' % default, '/', request.path_info)
        if settings.APPEND_SLASH and not language_path.endswith('/'):
            language_path = language_path + '/'

        if is_valid_path(language_path, urlconf):
            #we use a permanent redirect here.
            #when changing the default language we need to let the world know
            #that our links have permanently changed and transfer our seo juice
            #to the new url
            #http://blog.yawd.eu/2012/impact-django-page-redirects-seo/
            return  HttpResponsePermanentRedirect("%s://%s/%s" % (
                request.is_secure() and 'https' or 'http',
                request.get_host(), re.sub(r'^/%s/' % default, '', request.get_full_path())))

    patch_vary_headers(response, ('Accept-Language',))
    if 'Content-Language' not in response:
        response['Content-Language'] = language
    return response
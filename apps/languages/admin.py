from django.contrib import admin
from .models import Language


class LanguageAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'active')
    list_editable = ('active',)
    search_fields = ('name',)

admin.site.register(Language, LanguageAdmin)
""" A language tag model according to IETF RFC5646 https://tools.ietf.org/html/rfc5646
    data: https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry
    lookup: https://r12a.github.io/app-subtags/
"""
from django.db import models
from django.utils.translation import ugettext_lazy as _



class LanguageManager(models.Manager):

    def active(self, **kwargs):
        return self.filter(active=True, **kwargs)

    def default(self, **kwargs):
        return self.get(default=True, **kwargs)


class Language(models.Model):
    """
    Language tag has a following form:
        langtag = language[- script][- region]
    where
        language: 2(3) alpha code ISO639
        script: 4 alpha ISO 15924
        region: 2 alpha ISO 3166-1
    """
    DELIMITER = '-'
    id = models.CharField('Language code', max_length=20, primary_key=True)
    name = models.CharField('Language Name', max_length=128)
    active = models.BooleanField(_('Active'), default=False)
    default = models.NullBooleanField(unique=True)  # in this way only one record with True value is allowed
    countries = models.ManyToManyField('countries.Country')

    objects = LanguageManager()

    def __str__(self):
        return self.name

    @property
    def subtags(self):
        return self.id.split(self.DELIMITER)

    def language(self):
        return self.subtags[0]

    def script(self):
        tags = self.subtags
        if len(tags) == 3:
            return tags[1]

    def region(self):
        tags = self.subtags
        if len(tags) == 2:
            return tags[1]
        elif len(tags) == 3:
            return tags[2]

    class Meta(object):
        verbose_name = _('Language')
        verbose_name_plural = _('Languages')

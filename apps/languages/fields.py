from django.db.models import ForeignKey


class LanguageField(ForeignKey):
    
    def __init__(self, *args, to=None, **kwargs):
        super().__init__(to='languages.Language', *args, **kwargs)
        
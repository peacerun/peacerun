from django.db import models

# Create your models here.

class Route(models.Model):
    
    name = models.CharField(_('Name'), max_length=255)
    
    
class Event(models.Model):
    
    when = models.DateTimeField()
#!/bin/bash

NAME="newsite"                                    # Name of the application
HOME=/home/newsite
VENV=$HOME/.venv
DJANGODIR=$HOME/peacerun                          # Django project directory
SOCKFILE=/run/$NAME/gunicorn.sock                 # we will communicate using this unix socket
TIMEOUT=600                                       # timeout          
USER=newsite                                      # the user to run as
GROUP=newsite                                     # the group to run as
NUM_WORKERS=5                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=config.settings.production # which settings file should Django use
DJANGO_WSGI_MODULE=config.wsgi                    # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
source $VENV/bin/activate
cd $DJANGODIR
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $VENV/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --bind=unix:$SOCKFILE \
  --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE \
  --workers $NUM_WORKERS \
  --worker-class gevent \
  --timeout $TIMEOUT \
  --reload \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --log-file=-
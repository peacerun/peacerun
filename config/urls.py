""" Default urlconf for peacerun """

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.sitemaps.views import index, sitemap
from django.views.generic.base import TemplateView
from django.views.defaults import (permission_denied,
                                   page_not_found,
                                   server_error)
#from text.urls import urlpatterns as django_text_patterns


sitemaps = {
    # Fill me with sitemaps
}

admin.autodiscover()

urlpatterns = [
    #url(r'', include('base.urls')),

    # Admin
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Sitemap
    #url(r'^sitemap\.xml$', index, {'sitemaps': sitemaps}),
    #url(r'^sitemap-(?P<section>.+)\.xml$', sitemap, {'sitemaps': sitemaps}),

    # REST API
    #url(r'^api/cms/', include('cms.urls')),
    #url(r'^api-docs/', include_docs_urls(title='Peace Run API')),


 #   url(r'^django_text/', include(django_text_patterns, namespace='django_text')),

    # robots.txt
    url(r'^robots\.txt$',
        TemplateView.as_view(
            template_name='robots.txt',
            content_type='text/plain')
        ),
]

urlpatterns += [
    #i18n_patterns(
    url(r'^', include('pages.urls', namespace='pages')),
    #prefix_default_language=False)
]


if settings.DEBUG:
    # Add debug-toolbar
    import debug_toolbar  # noqa
    urlpatterns.append(url(r'^__debug__/', include(debug_toolbar.urls)))

    # Serve media files through Django.
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

    # Show error pages during development
    urlpatterns += [
        url(r'^403/$', permission_denied),
        url(r'^404/$', page_not_found),
        url(r'^500/$', server_error)
    ]
